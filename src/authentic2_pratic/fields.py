from django_select2 import AutoModelSelect2Field

from . import models

class CollectivityField(AutoModelSelect2Field):
    queryset = models.Collectivity.objects
    search_fields = ['name__icontains']
