__version__ = '1.0.0b3'

class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns

    def get_apps(self):
        return [__name__]

    def get_authentication_backends(self):
        return ['authentic2_pratic.backends.PraticSSLBackend',
                'authentic2_pratic.backends.PraticLoginPasswordSSLBackend',
                'authentic2_pratic.backends.PraticLoginPasswordBackend',
               ]


    def get_auth_frontends(self):
        return ['authentic2_pratic.auth_frontends.PraticFrontend']

    def get_after_middleware(self):
        return ('authentic2_pratic.middleware.PraticAuthMiddleware',)

    def get_attribute_backends(self):
        return ('authentic2_pratic.attribute_backend',)
