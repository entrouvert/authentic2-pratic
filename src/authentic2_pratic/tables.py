from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe

import django_tables2 as tables

from . import models

class ServiceTable(tables.Table):
    name = tables.TemplateColumn(
        '<a rel="popup" href="{% url "a2-pratic-service-edit" pk=record.pk %}">{{ record.name }}</a>',
        verbose_name=_('name'))
    delete = tables.TemplateColumn(
        '{% load i18n %}<a rel="popup" href="{% url "a2-pratic-service-delete" pk=record.pk %}">{% trans "Delete" %}</a>',
        verbose_name=mark_safe('&nbsp;'))

    class Meta:
        model = models.Service
        attrs = {'class': 'main', 'id': 'services-table'}
        fields = ('name', 'slug', 'is_global', 'service_url', 'delete')

class CollectivityTable(tables.Table):
    name = tables.TemplateColumn(
        '<a href="{% url "a2-pratic-collectivity-edit" collectivity_pk=record.pk %}">{{ record.name }}</a>',
        verbose_name=_('name'))
    delete = tables.TemplateColumn(
        '{% load i18n %}<a rel="popup" href="{% url "a2-pratic-collectivity-delete" collectivity_pk=record.pk %}">{% trans "Delete" %}</a>',
        verbose_name=mark_safe('&nbsp;'))

    class Meta:
        model = models.Collectivity
        attrs = {'class': 'main', 'id': 'collectivities-table'}
        fields = ('name', 'insee_code', 'postal_code', 'delete')

class UserTable(tables.Table):
    delete = tables.TemplateColumn(
        '{% load i18n %}<a rel="popup" href="{% url "a2-pratic-user-delete" collectivity_pk=record.collectivity.pk pk=record.pk %}">{% trans "Delete" %}</a>',
        verbose_name=mark_safe('&nbsp;'))

    class Meta:
        model = models.User
        attrs = {'class': 'main', 'id': 'users-table'}
        fields = ('first_name', 'last_name', 'uid', 'is_admin', 'is_active', 'delete')

class ServiceInstanceTable(tables.Table):
    slug = tables.TemplateColumn(
        '<a rel="popup" href="{% url "a2-pratic-service-instance-edit" collectivity_pk=record.collectivity.pk pk=record.pk %}">{{ record.slug}}</a>',
        verbose_name=_('identifier'))
    delete = tables.TemplateColumn(
        '{% load i18n %}<a rel="popup" href="{% url "a2-pratic-service-instance-delete" collectivity_pk=record.collectivity.pk pk=record.pk %}">{% trans "Delete" %}</a>',
        verbose_name=mark_safe('&nbsp;'))

    class Meta:
        model = models.User
        attrs = {'class': 'main', 'id': 'users-table'}
        fields = ('service', 'slug', 'service_url')

class AccessTable(tables.Table):
    last_name = tables.TemplateColumn('{{ record.user.last_name}}',
            verbose_name=_('Last name'), order_by=('user__last_name',))
    first_name = tables.TemplateColumn('{{ record.user.first_name }}',
            verbose_name=_('First name'), order_by=('user__first_name',))

    class Meta:
        model = models.Access
        attrs = {'class': 'main', 'id': 'accesses-table'}
        fields = ('last_name', 'first_name', 'service_instance')
