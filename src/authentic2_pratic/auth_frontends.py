import logging

from django.utils.translation import gettext_noop
from django.contrib.auth import authenticate
from django.shortcuts import render

from authentic2 import utils, views

from . import constants, forms, models

class PraticFrontend(object):
    def enabled(self):
        return True

    def id(self):
        return 'pratic'

    def name(self):
        return gettext_noop('Pr@tic')

    def login(self, request, *args, **kwargs):
        context_instance = kwargs.get('context_instance', None)
        # if there is only one ssl user, autolog him
        # only autologin every 10 minutes
        user = None
        how = None
        autologin = False
        if hasattr(request, 'ssl_info'):
            logger = logging.getLogger(__name__)
            logger.debug('ssl_info: %r', request.ssl_info.__dict__)
        if constants.PRATIC_AUTOLOGIN_COOKIE_NAME not in request.COOKIES:
            if hasattr(request, 'ssl_info') and request.ssl_info and \
                len(request.ssl_info.users) == 1:
                    user = authenticate(ssl_user=request.ssl_info.users[0])
                    how = 'ssl'
                    autologin = True
        # login SSL users by selection
        if not user and request.method == 'POST' and 'ssl-user' in request.POST \
                and hasattr(request, 'ssl_info') and request.ssl_info:
            try:
                ssl_user_id = int(request.POST['ssl-user'])
            except ValueError:
                pass
            else:
                try:
                    ssl_user = request.ssl_info.users.get(pk=ssl_user_id)
                except models.User.DoesNotExist:
                    pass
                else:
                    user = authenticate(ssl_user=ssl_user)
                    how = 'ssl'
        # now try to login using collectivity/login/password
        form = forms.AuthenticationForm(request=request, data=request.POST or None)
        if not user and request.method == 'POST' and 'pratic-login' in request.POST \
                and form.is_valid():
            how = form.get_how()
            if how == 'password' and request.is_secure():
                how += '-on-https'
            user = form.get_user()
        if user and how:
            response = utils.login(request, user, how)
            if autologin:
                response.set_cookie(constants.PRATIC_AUTOLOGIN_COOKIE_NAME,
                        max_age=constants.PRATIC_AUTOLOGIN_COOKIE_MAX_AGE)
            return response
        return render(request, 'authentic2_pratic/login.html',
                {'form': form}, context_instance=context_instance)

    def profile(self, request, *args, **kwargs):
        return views.login_password_profile(request, *args, **kwargs)
