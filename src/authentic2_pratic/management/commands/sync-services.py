from django.core.management.base import BaseCommand
from django.db import transaction

from authentic2_pratic.models import Service, ServiceInstance
from authentic2_pratic.utils import sync_saml_provider


class Command(BaseCommand):
    '''Load LDAP ldif file'''
    can_import_django_settings = True
    requires_model_validation = True
    help = 'Create SAML and OAuth2 configuration from services defs'

    @transaction.commit_on_success
    def handle(self, *args, **options):
        for service in Service.objects.all():
            sync_saml_provider(service)
        for service_instance in ServiceInstance.objects.all():
            sync_saml_provider(service_instance)

