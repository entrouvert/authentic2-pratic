from authentic2.managers import GetByNameManager
from authentic2.custom_user.managers import UserManager

class UserManager(UserManager):
    def get_by_natural_key(self, collectivity_slug, uid):
         return self.get(collectivity__slug=collectivity_slug, uid=uid)
                        
                       
