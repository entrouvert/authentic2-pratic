from django.conf.urls import patterns, url, include

from authentic2.manager import user_views as user_views
from . import views

service_instance_urlpatterns = patterns('',
        url('^$',
            views.service_instance_edit,
            name='a2-pratic-service-instance-edit'),
        url('^delete/$',
            views.service_instance_delete,
            name='a2-pratic-service-instance-delete'))

user_urlpatterns = patterns('',
        url('^$', views.user_edit, name='a2-pratic-user-edit'),
        url(r'change-password/$',
            user_views.user_change_password,
            name='a2-manager-user-change-password'),
        url('^delete/$', views.user_delete, name='a2-pratic-user-delete'))

collectivity_urlpatterns = patterns('',
        url('^$', views.collectivity_edit, name='a2-pratic-collectivity-edit'),
        url('^delete/$', views.collectivity_delete, name='a2-pratic-collectivity-delete'),
        url('^users/$', views.collectivity_users, name='a2-pratic-users'),
        url('^users/add/$',
            views.user_add,
            name='a2-pratic-user-add'),
        url('^users/(?P<pk>\d+)/',
            include(user_urlpatterns)),
        url('^services/$',
            views.collectivity_service_instances,
            name='a2-pratic-service-instances'),
        url('^services/add/$',
            views.service_instance_add,
            name='a2-pratic-service-instance-add'),
        url('^services/(?P<pk>\d+)/',
            include(service_instance_urlpatterns)),
        url('^accesses/$',
            views.collectivity_accesses, 
            name='a2-pratic-accesses'),
        )

urlpatterns = patterns('',
        url('^manage/$', views.homepage, name='a2-pratic-homepage'),
        url('^manage/services/$', views.services, name='a2-pratic-services'),
        url('^manage/services/add/$', views.service_add, name='a2-pratic-service-add'),
        url('^manage/services/(?P<pk>\d+)/delete/$', views.service_delete, name='a2-pratic-service-delete'),
        url('^manage/services/(?P<pk>\d+)/$', views.service_edit, name='a2-pratic-service-edit'),
        url('^manage/collectivities/$', views.collectivities, name='a2-pratic-collectivities'),
        url('^manage/collectivities/add/$', views.collectivity_add, name='a2-pratic-collectivity-add'),
        url('^manage/collectivities/(?P<collectivity_pk>\d+)/', include(collectivity_urlpatterns)),
        url('^manage/menu.json$', views.menu_json),
        url('^services.json$', views.agent_homepage_jsonp, name='auth_homepage_jsonp'),
        url('^user_info/$', views.user_info),
)
