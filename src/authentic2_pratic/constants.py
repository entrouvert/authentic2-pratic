from django.utils.translation import ugettext_lazy as _

AUTHENTICATION_LEVELS = (
    ('password-on-https', _('login/password')),
    ('ssl-collectivity', _('collectivity certificate + login/password')),
    ('ssl', _('X509 RGS certificate')),
)
PRATIC_AUTOLOGIN_COOKIE_NAME = 'pratic-autologin'
PRATIC_AUTOLOGIN_COOKIE_MAX_AGE = 60*10 # 10 minutes
PRATIC_AUTHN_CONTEXT_SSL_COLLECTIVITY = 'urn:cdg59.fr:names:tc:SAML:2.0:ac:classes:X509Collectivity'
