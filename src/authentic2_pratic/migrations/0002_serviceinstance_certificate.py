# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_pratic', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceinstance',
            name='certificate',
            field=models.BooleanField(default=False, verbose_name='Authentication by certificate only'),
            preserve_default=True,
        ),
    ]
