# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import authentic2.saml.fields


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_pratic', '0006_auto_20150622_1600'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='serviceinstance',
            name='certificate',
        ),
        migrations.AddField(
            model_name='serviceinstance',
            name='authentication_level',
            field=authentic2.saml.fields.MultiSelectField(default='password-on-https,ssl-collectivity,ssl', max_length=256, verbose_name='authentification level', choices=[(b'password-on-https', 'login/password'), (b'ssl-collectivity', 'collectivity certificate + login/password'), (b'ssl', 'X509 RGS certificate')]),
            preserve_default=True,
        ),
    ]
