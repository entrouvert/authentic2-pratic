# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2', '0007_auto_20150523_0028'),
        ('authentic2_pratic', '0005_auto_20150616_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='a2_service',
            field=models.ForeignKey(related_name='pratic_service', verbose_name='related authentic2 service', blank=True, to='authentic2.Service', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='serviceinstance',
            name='a2_service',
            field=models.ForeignKey(related_name='pratic_service_instances', verbose_name='related authentic2 service', blank=True, to='authentic2.Service', null=True),
            preserve_default=True,
        ),
    ]
