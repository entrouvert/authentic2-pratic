# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_pratic', '0004_auto_20150615_1851'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collectivity',
            name='sirh_label',
            field=models.CharField(max_length=64, verbose_name='SIRH Label', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='service',
            name='service_url',
            field=models.URLField(verbose_name='service base URL'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='service',
            name='slug',
            field=models.SlugField(unique=True, max_length=128, verbose_name='slug'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='serviceinstance',
            name='service_url',
            field=models.URLField(verbose_name='service base URL', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='serviceinstance',
            name='slug',
            field=models.SlugField(max_length=128, verbose_name='slug'),
            preserve_default=True,
        ),
    ]
