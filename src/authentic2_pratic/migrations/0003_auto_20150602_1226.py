# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_pratic', '0002_serviceinstance_certificate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serviceinstance',
            name='collectivity',
            field=models.ForeignKey(related_name='service_instances', verbose_name='collectivity', to='authentic2_pratic.Collectivity'),
            preserve_default=True,
        ),
    ]
