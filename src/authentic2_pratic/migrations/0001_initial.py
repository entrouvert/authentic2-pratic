# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('a2_rbac', '0005_auto_20150526_1406'),
        ('custom_user', '0006_auto_20150527_1212'),
    ]

    operations = [
        migrations.CreateModel(
            name='Access',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'ordering': ('user__last_name', 'user__first_name', 'service_instance__service__name'),
                'verbose_name': 'accesses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Collectivity',
            fields=[
                ('organizationalunit_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.RBAC_OU_MODEL)),
                ('is_superuser', models.BooleanField(default=False, verbose_name='is superuser')),
                ('collectivity_id', models.CharField(max_length=8, verbose_name='collectivity id', blank=True)),
                ('sirh_code', models.CharField(max_length=8, verbose_name='SIRH Code', blank=True)),
                ('sirh_label', models.CharField(max_length=64, verbose_name='SIRH Code', blank=True)),
                ('insee_code', models.CharField(max_length=8, verbose_name='INSEE Code', blank=True)),
                ('siret_code', models.CharField(max_length=32, verbose_name='SIRET Code', blank=True)),
                ('postal_address', models.TextField(verbose_name='postal address', blank=True)),
                ('street_number', models.CharField(max_length=8, verbose_name='street number', blank=True)),
                ('street', models.CharField(max_length=128, verbose_name='street', blank=True)),
                ('postal_code', models.CharField(max_length=16, verbose_name='postal code', blank=True)),
                ('address_complementary', models.CharField(max_length=64, verbose_name='complementary address', blank=True)),
                ('address_mention', models.CharField(max_length=64, verbose_name='address mention', blank=True)),
                ('arrondissement_code', models.CharField(max_length=64, verbose_name='arrondissement code', blank=True)),
                ('canton_code', models.CharField(max_length=4, verbose_name='canton code', blank=True)),
                ('departement_code', models.CharField(max_length=2, verbose_name='departement code', blank=True)),
                ('dist_office', models.CharField(max_length=64, verbose_name='distribution office', blank=True)),
                ('region_code', models.CharField(max_length=4, verbose_name='distribution office', blank=True)),
                ('phone', models.CharField(max_length=32, verbose_name='phone', blank=True)),
                ('fax', models.CharField(max_length=32, verbose_name='fax', blank=True)),
                ('email', models.EmailField(max_length=64, verbose_name='email', blank=True)),
                ('url', models.URLField(max_length=128, verbose_name='URL', blank=True)),
                ('certificate_issuer_dn', models.CharField(max_length=256, null=True, verbose_name='certificate issuer DN', blank=True)),
                ('certificate_subject_dn', models.CharField(max_length=256, null=True, verbose_name='certificate subject DN', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'collectivity',
                'verbose_name_plural': 'collectivities',
            },
            bases=('a2_rbac.organizationalunit',),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=128, verbose_name='name')),
                ('slug', models.SlugField(unique=True, max_length=128, verbose_name=b'identifier')),
                ('is_global', models.BooleanField(default=False, verbose_name='is global')),
                ('service_url', models.URLField(verbose_name='URL')),
                ('metadata_url', models.URLField(verbose_name='SAML Metadata URL', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'service',
                'verbose_name_plural': 'services',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServiceInstance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(max_length=128, verbose_name=b'identifier')),
                ('service_url', models.URLField(verbose_name='URL', blank=True)),
                ('metadata_url', models.URLField(verbose_name='SAML Metadata URL', blank=True)),
                ('collectivity', models.ForeignKey(verbose_name='collectivity', to='authentic2_pratic.Collectivity')),
                ('service', models.ForeignKey(verbose_name='service', to='authentic2_pratic.Service')),
            ],
            options={
                'ordering': ('service__name', 'slug'),
                'verbose_name': 'service instances',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('uid', models.CharField(max_length=128, verbose_name='identifier')),
                ('is_admin', models.BooleanField(default=False, verbose_name='is admin')),
                ('sirh_code', models.CharField(max_length=32, verbose_name='SIRH Code', blank=True)),
                ('direction', models.CharField(max_length=128, verbose_name='direction', blank=True)),
                ('last_login_duration', models.IntegerField(default=0, verbose_name='last connection duration', blank=True)),
                ('employee_type', models.CharField(max_length=128, verbose_name='employee type', blank=True)),
                ('postal_address', models.TextField(verbose_name='postal address', blank=True)),
                ('fax', models.CharField(max_length=32, verbose_name='fax', blank=True)),
                ('mobile', models.CharField(max_length=16, verbose_name='mobile', blank=True)),
                ('phone', models.CharField(max_length=32, verbose_name='phone', blank=True)),
                ('certificate_issuer_dn', models.CharField(max_length=256, null=True, verbose_name='certificate issuer DN', blank=True)),
                ('certificate_subject_dn', models.CharField(max_length=256, null=True, verbose_name='certificate subject DN', blank=True)),
                ('collectivity', models.ForeignKey(related_name='collectivities', verbose_name='collectivity', to='authentic2_pratic.Collectivity')),
            ],
            options={
                'verbose_name': 'agent',
                'verbose_name_plural': 'agents',
            },
            bases=('custom_user.user',),
        ),
        migrations.AlterUniqueTogether(
            name='user',
            unique_together=set([('uid', 'collectivity')]),
        ),
        migrations.AlterUniqueTogether(
            name='serviceinstance',
            unique_together=set([('slug', 'collectivity')]),
        ),
        migrations.AlterUniqueTogether(
            name='collectivity',
            unique_together=set([('certificate_issuer_dn', 'certificate_subject_dn')]),
        ),
        migrations.AddField(
            model_name='access',
            name='service_instance',
            field=models.ForeignKey(verbose_name='service instance', to='authentic2_pratic.ServiceInstance'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='access',
            name='user',
            field=models.ForeignKey(verbose_name='user', to='authentic2_pratic.User'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='access',
            unique_together=set([('user', 'service_instance')]),
        ),
    ]
