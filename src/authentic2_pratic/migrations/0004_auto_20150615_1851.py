# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_pratic', '0003_auto_20150602_1226'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='cas_service_url',
            field=models.URLField(verbose_name='CAS service URL', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='serviceinstance',
            name='cas_service_url',
            field=models.URLField(verbose_name='CAS service URL', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='serviceinstance',
            name='service',
            field=models.ForeignKey(related_name='service_instances', verbose_name='service', to='authentic2_pratic.Service'),
            preserve_default=True,
        ),
    ]
