from . import utils

class PraticAuthMiddleware(object):
    """
    attempts to find a valid user based on the client certificate info
    """
    def process_request(self, request):
        request.ssl_info  = utils.SSLInfo(request)
