import logging

from django.contrib.auth.backends import ModelBackend

from . import models, constants

class BaseBackend(ModelBackend):
    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        super(BaseBackend, self).__init__(*args, **kwargs)

    def get_user(self, user_id):
        try:
            return models.User._default_manager.get(pk=user_id)
        except models.User.DoesNotExist:
            return None

class PraticLoginPasswordBackend(BaseBackend):
    def authenticate(self, collectivity, username, password):
        try:
            user = models.User.objects.select_related().get(collectivity=collectivity, uid=username)
        except models.User.DoesNotExist:
            pass
        else:
            if user.check_password(password):
                return user

    def get_saml2_authn_context(self, **kwargs):
        import lasso
        request = kwargs.pop('request', None)
        if request and request.is_secure():
            return lasso.SAML2_AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT
        else:
            return lasso.SAML2_AUTHN_CONTEXT_PASSWORD

class PraticSSLBackend(BaseBackend):
    def authenticate(self, ssl_user, **kwargs):
        return ssl_user

    def get_saml2_authn_context(self, **kwargs):
        import lasso
        return lasso.SAML2_AUTHN_CONTEXT_X509

class PraticLoginPasswordSSLBackend(PraticLoginPasswordBackend):
    def authenticate(self, collectivity, username, password, ssl_info):
        user = super(PraticLoginPasswordSSLBackend, self).authenticate(collectivity, username, password)
        if not ssl_info.collectivity or collectivity not in list(ssl_info.collectivity):
            return
        try:
            user = models.User.objects.select_related().get(collectivity=collectivity, uid=username)
        except models.User.DoesNotExist:
            pass
        else:
            if user.check_password(password):
                return user

    def get_saml2_authn_context(self, **kwargs):
        return constants.PRATIC_AUTHN_CONTEXT_SSL_COLLECTIVITY
