from django.utils.translation import ugettext_lazy as _

from admin_tools.dashboard import modules


def get_admin_modules():
    '''Show models in authentic2 admin'''
    model_list = modules.ModelList(_('Pr@tic'),
            models=('authentic2_pratic.models.*',))
    return (model_list,)

