from django.utils.translation import ugettext_lazy as _

from authentic2.models import Attribute, AttributeValue

from authentic2.decorators import to_list
from django.contrib.auth import get_user_model

from . import models

@to_list
def get_instances(ctx):
    '''
    Retrieve instances from settings
    '''
    return [None]

@to_list
def get_attribute_names(instance, ctx):
    for field in models.User._meta.local_fields:
        if field.name == 'user_ptr':
            continue
        yield 'pratic_' + field.name, field.verbose_name
    yield 'pratic_cn', 'Nom complet'

def get_dependencies(instance, ctx):
    return ('user',)

def get_attributes(instance, ctx):
    user = ctx.get('user')
    if not isinstance(user, models.User):
        return ctx

    for field in models.User._meta.local_fields:
        if field.name == 'user_ptr':
            continue
        ctx['pratic_' + field.name] = getattr(user, field.name)
    ctx['pratic_cn'] = user.first_name + ' ' + user.last_name
    return ctx
