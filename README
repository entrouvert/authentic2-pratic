Authentic2 Pr@tic
=================

- Ajout d'un frontal d'authentification spécifique pour la gestion de l'authentification deux facteurs PKI + mot de passe
- Remplacement de la gestion des utilisateurs et des groupes d'Authentic2 par
  - gestion des collectivités
  - gestion des utilisateurs
  - gestion des services
  - gestion des accréditations
- Ajout d'un handler du signal d'autorisation pour interdire l'accès aux
  services sauf aux utilisateurs explicitements autorisés

Modèles
=======

authentic2_pratic.models.Collectivity
-------------------------------------

Le champ booléen `is_superuser` indique si les utilisateurs de cette
collectivité sont des super-administrateurs. Quand ce champ a la valeur `True`
tous les utilisateurs de cette collectivité reçoivent la valeur `True` dans
leur champs `is_superuser`.

authentic2_pratic.models.User
-----------------------------

Il hérite du modèle `django.contrib.auth.models.User` il contient les
données utilisateur spécifiques au CDG59. Il est lié à l'objet `Collectivity`
via le champs de clé étrangère `collectivity`.

authentic2_pratic.models.Service
--------------------------------

Il décrit un service proposé par le CDG59. Ce service peut
éventuellement utiliser l'authentification d'authentic via le protocole
SAML 2.0.

Les champs `name`, `slug` et `service_url` sont obgligatoires. Le champs
`service_url` indique l'URL de base pour accéder au service, c'est l'URL qui
sera affichée aux utilisateurs.

Dans le cas SAML 2.0 le champ `metadata_url` contiendra l'URL des métadonnées
du service.

Le champ booléen `is_global` indique si un service est global, dans ce cas les
champs `service_url` et 'metadata_url` des instances de ce service seront
ignorés et ceux de la définition du service seront utilisés.

authentic2_pratic.models.ServiceInstance
----------------------------------------

Il permet de déclarer un service au niveau d'une collectivité, les
champs sont les mêmes que pour un service à l'exception du champ `is_global`
qui est absent. Pour pouvoir donner accès à un service à un utilisateur il est
impératif d'en créer une instance lié à sa collectivité, créer un service n'est
pas suffisant.

authentic2_pratic.models.Access
-------------------------------

Il indique l'autorisation pour un utilisateur d'accèder à une instance de
service ou simplement de voir ce service dans sa page d'accueil dans le cas
d'un service sans authentification lié à authentic.

Il est lié à un utilisateur via le champ `user` et à une instance de service
via le champ `service_instance`. La simple existence de cet objet indique
l'ouverture de ce service à cet utilisateur.

Gestion des autorisations
=========================

Les super-utilisateurs sont autorisés à se connecter à tous les services, les
autres utilisateurs doivent être manuellement autorisés au niveau de leur
collectivité dans le panneau de gestion des accréditations.

URLs
====

`/manage/`
----------

Pour un super-administrateur, elle contient un lien vers la gestion des
collectivités et vers la gestion des services.

Pour un administrateur de collectivité elle contient trois liens profond vers
la gestion des utilisateurs, des instances de service et des accréditations
pour cette collectivité.

- `/manage/services/`: liste des services
- `/manage/services/add/`: ajout d'un service (popup)
- `/manage/services/<id>/`: édition d'un service (popup)
- `/manage/services/<id>/delete/`: suppression d'un service (popup)
- `/manage/collectivities/`: liste des collectivités
- `/manage/collectivities/add/`: ajout d'une collectivité
- `/manage/collectivitiss/<id>/`: édition d'une collectivité
- `/manage/collectivities/<id>/delete`: suppression d'une collectivité
- `/manage/collectivities/<id>/users/`: agents d'une collectivité
- `/manage/collectivities/<id>/users/add/`: ajout d'un agent à une collectivité (popup)
- `/manage/collectivities/<id>/users/<pk>/`: édition d'un agent d'une collectivity (popup)
- `/manage/collectivities/<id>/users/<pk>/delete/`: suppression d'un agent d'une collectivity (popup)
- `/manage/collectivities/<id>/services/`: instances de service d'un collectivité
- `/manage/collectivities/<id>/services/add/`: ajout d'unee instance de service à une collectivité (popup)
- `/manage/collectivities/<id>/services/<pk>/`: édition d'une instance de service d'une collectivity (popup)
- `/manage/collectivities/<id>/services/<pk>/delete/`: suppression d'une instance de service d'une collectivity (popup)
- `/manage/collectivities/<id>/accesses/`: accréditations d'une collectivité, i.e. autorisations d'accès
- `/manage/collectivities/<id>/accesses/add/`: ajout d'un accès (popup)
- `/manage/collectivities/<id>/accesses/<pk>/`: édition d'un accès (popup)
- `/manage/collectivities/<id>/accesses/<pk>/`: suppression d'un accès (popup)

Authentification X509
=====================

Un certificat X509 peut être associé à au maximum une collectivité et
plusieurs utilisateurs. 

Si un utilisateur présente un certificat de
collectivité, il peut se connecter avec son login et son mot de passe avec un
niveau d'authentification supérieur, nommé 'ssl-collectivity' au niveau
d'authentic et ayant le code SAML suivant:;

    urn:cdg59.fr:names:tc:SAML:2.0:ac:classes:X509Collectivity

Si un utilisateur présente un certificat lié à un utilisateur il est
automatiquement connecté avec un niveau d'authentification 'ssl'. Après une
première auto-connexion, l'auto-connexion est désactivé durant 10 minutes,
pendant ce temps l'utilisateur pourra se déconnecter puis se reconnecter en
utilisant une des autres méthodes d'authentification.

L'association d'un certificat se fait en recopiant dans les champs
`certificate_issuer_dn` et `certificate_subject_dn` des objets
`authentic2_pratic.models.Collectivity` ou `authentic2_pratic.models.User` les
nom distingués de l'émetteur et du sujet du certificat tels qu'ils sont
exportés par le serveur web hébergeant authentic

Le nom des clés d'environnement contenant les noms distingués de l'émetteur et
du sujet du certificat sont configurés dans le fichier
`authentic2_pratic/app_settings.py`.  Ils sont adaptés à une utilisant
d'authentic avec mod_wsgi, mod_scgi, mot_python ou mod_fcig sous Apache ou bien
les modules SCGI ou FCGI de NGinx. Si authentic est lancé dans un conteneur
python comme gunicorn, les variables d'environnement SSL devront être recopié
dans des entêtes HTTP et ces entêtes HTTP déclaré dans la configuration
d'authentic. Par exemple avec nginx on pourra utiliser cette configuration au
niveau du virtualhost::

    ssl_verify_client optional;
    ssl_client_certificate autorites_de_certifications_autorises.pem;

    location / {
        proxy_pass http://localhost:8000;
        proxy_set_header ssl-client-dn $ssl_client_s_dn;
        proxy_set_header ssl-issuer-dn $ssl_client_i_dn;
        proxy_set_header ssl-client-verify $ssl_client_verify;
    }

et la configuration suivante dans le fichier `config.py` d'authentic::

    A2_PRATIC_X509_KEYS = {
            'subject_dn': 'HTTP_SSL_CLIENT_DN',
            'issuer_dn': 'HTTP_SSL_ISSUER_DN',
            'verify': 'HTTP_SSL_CLIENT_VERIFY',
    }

Dans les formulaires d'édition d'un utilisateur ou d'un collectivité il est
possible de configurer les champs concernant les noms distingués en
téléchargeant directement le certificat de cette utilisateur, OpenSSL sera
utilisé pour analyser le certificat et en extraire les valeurs des noms
distingués.

ATTENDION: la simple configuration des noms distingués d'un certificat n'est
pas une sécurité, il faut en plus que la chaîne de confiance du certificat ait
été préalablement validée par la terminaison SSL --- généralement le serveur
Web, pour nginx en utilisant les directives `ssl_verify_client` avec les
valeurs `optional` ou FIXME et en définissant les certificats racines des
autorités de certificats de confiance via la directive
`ssl_client_certificate`.
